package ee.bcs.valiit.person.model;

import lombok.Data;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.Period;

@Data
public class Person {

    private String name;

    private Integer age;

    private Double length;

    private Double weight;

    private Sex sex;

    public Person() {}

    public Person(String name, String idCode) throws Exception {
        if (validatePersonalCode(new BigInteger(idCode))) {
            this.name = name;
            sex = deriveGender(idCode);
            age = calculateAge(idCode);
        } else {
            throw new Exception("ID code is invalid!");
        }
    }

    private boolean validatePersonalCode(final BigInteger idCode) {
        String idCodeStr = idCode.toString();
        int[] weights1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1};
        int check = 0;
        for (int i = 0; i < weights1.length; i++) {
            check += Integer.parseInt(idCodeStr.substring(i, i + 1))
                    * weights1[i];
        }
        int temp = check % 11;
        String idCodeCheck = idCodeStr.substring(10, 11);
        if (temp != 10) {
            return Integer.parseInt(idCodeCheck) == temp;
        } else {
            int[] weights2 = {3, 4, 5, 6, 7, 8, 9, 1, 2, 3};
            for (int i = 0; i < weights2.length; i++) {
                check += Integer.parseInt(idCodeStr.substring(i, i + 1))
                        * weights2[i];
            }
            temp = check % 11;
            if (temp != 10) {
                return Integer.parseInt(idCodeCheck) == temp;
            } else {
                return Integer.parseInt(idCodeCheck) == 0;
            }
        }
    }

    private Sex deriveGender(String idCode) {
        return (int) idCode.charAt(0) % 2 == 0 ? Sex.WOMAN : Sex.MAN;
    }

    private Integer calculateAge(String idCode) {
        int day = Integer.parseInt(idCode.substring(5, 7));
        int month = Integer.parseInt(idCode.substring(3, 5));
        int year = deriveBirthYear(idCode);
        LocalDate birthday = LocalDate.of(year, month, day);
        Period age = Period.between(birthday, LocalDate.now());
        return age.getYears();
    }

    private int deriveBirthYear(String idCode) {
        char sajand = idCode.charAt(0);
        StringBuilder synniaasta = new StringBuilder();
        switch (sajand) {
            case '1':
            case '2':
                synniaasta.append("18");
                break;
            case '3':
            case '4':
                synniaasta.append("19");
                break;
            case '5':
            case '6':
                synniaasta.append("20");
                break;
            case '7':
            case '8':
                synniaasta.append("21");
                break;
            default:
                synniaasta.append("Pole võimalik :)");
        }
        synniaasta.append(idCode.substring(1, 3));
        return Integer.parseInt(synniaasta.toString());
    }
}
