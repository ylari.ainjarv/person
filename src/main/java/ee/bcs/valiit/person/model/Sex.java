package ee.bcs.valiit.person.model;

public enum Sex {
    WOMAN, MAN, UNKNOWN
}
