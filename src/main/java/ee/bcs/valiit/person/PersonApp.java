package ee.bcs.valiit.person;

import ee.bcs.valiit.person.model.Person;

public class PersonApp {

    public static void main(String[] args) {
        try {
            Person mati = new Person("Mati", "37212210230");
            mati.setLength(180.0);
            mati.setWeight(100.00);
            System.out.println(mati.getName() + " is " + mati.getSex() + ", age " + mati.getAge());
            Person kati = new Person("Kati", "47212210231");
            kati.setLength(178.5);
            System.out.println(kati.getName() + " is " + kati.getSex());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

}
